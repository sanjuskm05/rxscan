import React, { Component } from "react";
import { Image, View, StatusBar } from "react-native";

import { Container, Content, Item, Input, Button, Icon,
	 Text, Picker, Spinner } from 'native-base';
import axios from 'axios';
import styles from "./styles";

const krogerPharmacyScreenBackground = require('../../../img/krogerPharmacy.jpg');
const refillScreenBackground = require('../../../img/refill.png');
const appConfig = require('../../app-config.json');

class Login extends Component {
	constructor(props) {
		super(props);
	}

  onScanPress() {
    this.props.navigation.navigate('Home');
  }

	render() {
		return (
				<Container>
					<Image source={refillScreenBackground} style={styles.imageContainer}>
						<Content style={styles.contentContainer}>
							<Button medium iconLeft rounded success style={styles.btn} onPress={() => { this.onScanPress(); }} >
								<Icon name="camera" />
								<Text>Scan</Text>
							</Button>
						</Content>
					</Image>
				</Container>
		);
	}
}

export default Login;
