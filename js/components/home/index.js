import React, { Component } from "react";
import { TouchableOpacity, Alert, ScrollView, Platform, Image } from 'react-native';

import { Container, Header, Title, Content, View, Text, Button, Icon, Left, Body, Picker,
  Item, List, ListItem, InputGroup, Input, Right, Footer, FooterTab, Thumbnail, Spinner } from 'native-base';

import RNTesseractOcr from 'react-native-tesseract-ocr';
import Camera from 'react-native-camera';

import styles from "./styles";

const appConfig = require('../../app-config.json');

class Home extends Component {

  render() {
    return(
      <View style={styles.cameraContainer}>
        <Camera
         ref = {(cam)=>{
           this.camera = cam
         }}
            style={styles.cameraPreview}>
            <TouchableOpacity
                style={styles.captureButton}
                onPress={this.capture.bind(this)}
            >
              <Image
                  source={require('../../../img/assets/ic_photo_camera_36pt.png')}
              />
            </TouchableOpacity>
        </Camera>
      </View>
    );
  }
  capture() {
    let path = "";

    this.camera.capture().done((data)=> {
      path = data.path.replace;
      console.log(data.path);
      RNTesseractOcr.startOcr(path, "LANG_ENGLISH")
        .then((result) => {
          this.setState({ ocrResult: result });
          console.log("OCR Result: ", result);
        })
        .catch((err) => {
          console.log("OCR Error: ", err);
        })
        .done((data)=>{
          console.log(data);
        });
    });

  }
}

export default Home;
